package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBHelper {

    public Connection getConnection() throws SQLException, DBException {
        String URL;
        try {
            Properties p = new Properties();
            p.load(new FileInputStream("app.properties"));
            URL = (String) p.get("connection.url");
        }
        catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
        return DriverManager.getConnection(URL);
    }
}
