package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static final String SQL_INSERT_USER = "INSERT INTO users (login) VALUES (?)",
								SQL_DELETE_USER = "DELETE FROM users WHERE login = ?",
								SQL_SELECT_USER = "SELECT ID, LOGIN FROM users WHERE login = ?",
								SQL_SELECT_ALL_USERS = "SELECT * FROM users";

	private static final String SQL_INSERT_TEAM = "INSERT INTO teams (name) VALUES (?)",
								SQL_DELETE_TEAM = "DELETE FROM teams WHERE name = ?",
								SQL_SELECT_TEAM = "SELECT ID, NAME FROM teams WHERE name = ?",
								SQL_SELECT_ALL_TEAMS = "SELECT * FROM teams",
								SQL_UPDATE_TEAM = "UPDATE teams SET name = ? WHERE name = ?",
								SQL_SELECT_USER_TEAMS = "SELECT teams.id, teams.name FROM users, users_teams, teams WHERE users.id = users_teams.user_id AND teams.id = users_teams.team_id AND users.login = ?";

	private static final String SQL_SET_TEAM_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES(?, ?)";

	private Connection connection;
	private DBHelper helper;

	public static synchronized DBManager getInstance() {
		if(instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		helper = new DBHelper();
	}

	public List<User> findAllUsers() throws DBException {
		try {
			connection = helper.getConnection();
			PreparedStatement allUsersStatement = connection.prepareStatement(SQL_SELECT_ALL_USERS);
			ResultSet resultSet = allUsersStatement.executeQuery();

			List<User> allUsers = new ArrayList<>();
			while(resultSet.next()) {
				allUsers.add(new User(resultSet.getInt(1), resultSet.getString(2)));
			}
			resultSet.close();
			connection.close();
			return allUsers;
		}
		catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean insertUser(User user) throws DBException {
		try {
			connection = helper.getConnection();
			PreparedStatement insertUserStatement = connection.prepareStatement(SQL_INSERT_USER);
			insertUserStatement.setString(1, user.getLogin());
			boolean statementExecuted = insertUserStatement.execute();
			connection.close();
			return statementExecuted;
		}
		catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		try(Connection connection = helper.getConnection();) {
			for(User user : users) {
				PreparedStatement deleteUserStatement = connection.prepareStatement(SQL_DELETE_USER);
				deleteUserStatement.setString(1, user.getLogin());
				int statementExecuted = deleteUserStatement.executeUpdate();
				if(statementExecuted < 1) {
					throw new SQLException();
				}
			}
		}
		catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		}
		return false;
	}

	public User getUser(String login) throws DBException {
		try(Connection connection = helper.getConnection();) {
			PreparedStatement getUserStatement = connection.prepareStatement(SQL_SELECT_USER);
			getUserStatement.setString(1, login);
			ResultSet resultSet = getUserStatement.executeQuery();
			resultSet.next();
			return new User(resultSet.getInt(1), resultSet.getString(2));
		}
		catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public Team getTeam(String name) throws DBException {
		try(Connection connection = helper.getConnection();) {
			PreparedStatement getUserStatement = connection.prepareStatement(SQL_SELECT_TEAM);
			getUserStatement.setString(1, name);
			ResultSet resultSet = getUserStatement.executeQuery();
			resultSet.next();
			return new Team(resultSet.getInt(1), resultSet.getString(2));
		}
		catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		try {
			connection = helper.getConnection();
			PreparedStatement allUsersStatement = connection.prepareStatement(SQL_SELECT_ALL_TEAMS);
			ResultSet resultSet = allUsersStatement.executeQuery();
			List<Team> allTeams = new ArrayList<>();
			while(resultSet.next()) {
				allTeams.add(new Team(resultSet.getInt(1), resultSet.getString(2)));
			}
			connection.close();
			return allTeams;
		}
		catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		try {
			connection = helper.getConnection();
			PreparedStatement insertTeamStatement = connection.prepareStatement(SQL_INSERT_TEAM);
			insertTeamStatement.setString(1, team.getName());
			boolean statementExecuted = insertTeamStatement.execute();
			connection.close();
			return statementExecuted;
		}
		catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		try {
			connection = helper.getConnection();
			connection.setAutoCommit(false);
			User u = getUser(user.getLogin());
			for(Team team : teams) {
				Team t = getTeam(team.getName());
				PreparedStatement insertTeamStatement = connection.prepareStatement(SQL_SET_TEAM_FOR_USER);
				insertTeamStatement.setInt(1, u.getId());
				insertTeamStatement.setInt(2, t.getId());
				int statementExecuted = insertTeamStatement.executeUpdate();
			}
			connection.commit();
			connection.close();
			return true;
		}
		catch (Exception e) {
			if (connection != null) {
				try {
					connection.rollback();
					throw new DBException(e.getMessage(), e);
				}
				catch (SQLException exception) {
					throw new DBException(e.getMessage(), e);
				}
			}
		}
		return false;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		try(Connection connection = helper.getConnection()) {
			PreparedStatement allUsers = connection.prepareStatement(SQL_SELECT_USER_TEAMS);
			allUsers.setString(1, user.getLogin());
			ResultSet resultSet = allUsers.executeQuery();
			List<Team> allTeams = new ArrayList<>();
			while(resultSet.next()) {
				allTeams.add(new Team(resultSet.getInt(1), resultSet.getString(2)));
			}
			return allTeams;
		}
		catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		try {
			connection = helper.getConnection();
			PreparedStatement deleteTeamStatement = connection.prepareStatement(SQL_DELETE_TEAM);
			deleteTeamStatement.setString(1, team.getName());
			boolean statementExecuted = deleteTeamStatement.execute();
			connection.close();
			return statementExecuted;
		}
		catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try {
//			System.out.println(team.getOldName() + "  " + team);
//			List<Team> teamList = findAllTeams();
//			for(Team t : teamList) {
//				System.out.println(t.getId() + "  " + t);
//			}
			connection = helper.getConnection();
			PreparedStatement updateTeamStatement = connection.prepareStatement(SQL_UPDATE_TEAM);
			updateTeamStatement.setString(1, team.getName());
			updateTeamStatement.setString(2, team.getOldName());
			int statementExecuted = updateTeamStatement.executeUpdate();
			connection.close();
			return statementExecuted > 0;
		}
		catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		}
	}

}
