package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;
	private static int staticID = 0;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public User(){
		staticID++;
	}

	public User(int id, String login) {
		this.id = id;
		staticID++;
		this.login = login;
	}

	public static User createUser(String login) {
		return new User(staticID, login);
	}

	@Override
	public String toString() {
		return login;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		return login.equals(user.login);
	}

	@Override
	public int hashCode() {
		return Objects.hash(login);
	}
}
