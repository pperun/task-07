package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

	private int id;
	private static int staticID = 0;
	private String oldName;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getOldName() {
		return oldName;
	}

	public void setName(String name) {
		this.oldName = this.name;
		this.name = name;
	}

	public Team(){
		staticID++;
	}

	public Team(int id, String name) {
		this.id = id;
		staticID++;
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(staticID, name);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Team team = (Team) o;
		return name.equals(team.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}
}
